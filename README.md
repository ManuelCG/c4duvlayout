# C4D UV Layout Plugin
C4D UV Layout its a plugin that adds Headus UV Layout support for Cinema 4D,
it allows you to send polygons back and forth from Cinema 4D to UV Layout

It tries to mimic how the UV Layout Maya plugin works

Currently, the plugin was only tested on __Windows__

## How to setup?

To install the plugin in Cinema 4D follow the steps:

1. Download the latest version from the [Downloads](https://bitbucket.org/ManuelCG/c4duvlayout/downloads/) section

2. Then place the c4d_uvlayout folder inside "MAXON\CINEMA 4D R18\plugins"
	
3. Finally open Cinema 4D, if everything went right you should see "C4D UV Layout" in the plugin tab

## Usage:

### How to start up:
Run Cinema 4D, go to the plugin tab and select __C4D UV Layout__. Next, click on the __Run__ button to launch UVLayout and check 
the __Load Options__ that you want.

### Sending objects to UVLayout:
Select one or more objects on Cinema 4D, then click on the __Send__ button, the objects should load on UVLayout after a second or two. All 
objects sent to UVLayout __must__ be polygons.

### Sending objects back to Cinema 4D:
Just click on UVLayout's __Send__ button, the green one. Cinema 4D will create a new UVW Tag for the objects sent previously.

### Best way to exit:
When you're done opeing the UVs, click on the __Close__ button. This will close UVLayout and also the Thread inside Cinema 4D.

## License

> __MIT__
>
> Copyright (c) 2017 Manuel Canavarros Girard
>
> Permission is hereby granted, free of charge, to any person obtaining
> a copy of this software and associated documentation files (the "Software"),
> to deal in the Software without restriction, including without limitation
> the rights to use, copy, modify, merge, publish, distribute, sublicense,
> and/or sell copies of the Software, and to permit persons to whom the
> Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included
> in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
> OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
> DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
> TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
> OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


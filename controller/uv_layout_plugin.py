#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
file: uvlayout_plugin.py
author: Manuel Canavarros Girard
email: manuel.canavarros@gmail.com
github: https://bitbucket.org/manuelcanavarrosgirard/
description: This plugin adds support for UV Layout on cinema 4d
version: 1.0

Copyright © 2017 Manuel Canavarros

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""


import logging
import sys
import os

import c4d
from c4d import gui

# Configure Logger
LOGGER = logging.getLogger(__name__)
_HANDLER = logging.StreamHandler()
_FORMAT = logging.Formatter(
    '%(asctime)s - uvlayout_plugin - %(levelname)s - %(message)s')
_HANDLER.setFormatter(_FORMAT)
LOGGER.addHandler(_HANDLER)
LOGGER.setLevel(logging.DEBUG)

# Adding uvlayout_model to sys.path
__root__ = os.path.dirname(__file__)
LOGGER.debug('Trying to add c4d_uvlayout.model to sys path')
if os.path.join(__root__, 'model') not in sys.path:
    sys.path.insert(0, os.path.join(__root__, 'model'))
    LOGGER.debug("Added to sys path %s", os.path.join(__root__, 'model'))

from c4d_uvlayout import C4DUVLayout
from c4d_utils import getExePath

PLUGIN_ID = 1040002
BUTTON_RUN = 50057
BUTTON_SEND = 50058
BUTTON_CLOSE = 50059
GROUP_OPTIONS = 20000


class PluginUI(gui.GeDialog):
    """
    Creates the UI for C4D UV Layout Plugin
    It contains 3 buttons, Run, Send and Close
    """

    def __init__(self):
        _path = getExePath()
        self.uv_layout = C4DUVLayout(_path)

    def CreateLayout(self):
        """
        Setup layout for the plugin
        """

        self.SetTitle("Cinema 4D UV Layout")
        self.GroupBegin(GROUP_OPTIONS, c4d.BFH_SCALE, 1, 2)
        self.GroupSpace(30, 30)
        self.AddButton(BUTTON_RUN, c4d.BFH_CENTER, 80, 30, name="Run")
        self.AddButton(BUTTON_SEND, c4d.BFH_CENTER, 80, 30, name="Send")
        self.AddButton(BUTTON_CLOSE, c4d.BFH_CENTER, 80, 30, name="Close")
        self.GroupEnd()
        return True

    def Command(self, id, msg):
        """
        Handle the buttons using the respective ID

        :returns (bool): True if successful
        """

        if id == BUTTON_RUN:
            if (self.uv_layout.uv_process is None
                    or self.uv_layout.uv_process.poll() is not None):
                try:
                    self.uv_layout.run()
                except TypeError:
                    _path = getExePath()
                    self.uv_layout.uv_path = _path
                    return False

            else:
                LOGGER.warning("UV Layout it's already running")

        elif id == BUTTON_SEND:
            self.uv_layout.send()

        elif id == BUTTON_CLOSE:
            if self.uv_layout.uv_process is not None:
                if self.uv_layout.uv_process.poll() is None:
                    self.uv_layout.endWatcher()

        return True


class UVLayoutPlugin(c4d.plugins.CommandData):

    """Register C4D UV Layout plugin in C4D"""

    def __init__(self):
        self.plugin_ui = None

    def Register(self):
        """Register the plugin on cinema 4D"""

        help_string = ("Start C4D UV Layout UI")
        icon = c4d.bitmaps.BaseBitmap()
        icon.InitWith(os.path.join(
            os.path.dirname(__file__),
            "res",
            "icon.tif")
                     )
        return c4d.plugins.RegisterCommandPlugin(
            PLUGIN_ID,
            "C4D UV Layout",
            0,
            icon,
            help_string,
            self,
        )

    def Execute(self, doc):
        if self.plugin_ui is None:
            self.plugin_ui = PluginUI()
        return self.plugin_ui.Open(
            c4d.DLG_TYPE_ASYNC,
            defaultw=300,
            defaulth=50,
            pluginid=PLUGIN_ID
            )

    def RestoreLayout(self, sec_ref):
        if self.plugin_ui is None:
            self.plugin_ui = PluginUI()
        return self.plugin_ui.Restore(pluginid=PLUGIN_ID, secret=sec_ref)


if __name__ == "__main__":
    if UVLayoutPlugin().Register():
        LOGGER.info("Initialized C4D UV Layout Plugin")

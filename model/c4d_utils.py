#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: c4d_utils
Author: Manuel Canavarros
Email: manuel.canavarros@gmail.com
Github: https://bitbucket.org/ManuelCanavarrosGirard/
Description: Cinema 4D utility functions

Copyright © 2017 Manuel Canavarros

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import logging
import os

import c4d

# Configure Logger
LOGGER = logging.getLogger(__name__)
_HANDLER = logging.StreamHandler()
_FORMAT = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
_HANDLER.setFormatter(_FORMAT)
LOGGER.addHandler(_HANDLER)
LOGGER.setLevel(logging.DEBUG)


def exportSelection(path):
    """Export selected objects to path

    :path(str): String containing the path to export
    :return (bool): True if successful

    """
    doc = getDoc()
    objs = doc.GetActiveObjects(c4d.GETACTIVEOBJECTFLAGS_CHILDREN)
    if not objs:
        return False

    doc_temp = c4d.documents.IsolateObjects(doc, objs)
    if not doc_temp:
        return False

    export_plug = c4d.plugins.FindPlugin(1030178, c4d.PLUGINTYPE_SCENESAVER)
    if export_plug is None:
        return False

    op_export = {}
    # Send MSG_RETRIEVEPRIVATEDATA to OBJ export plugin
    if export_plug.Message(c4d.MSG_RETRIEVEPRIVATEDATA, op_export):
        if "imexporter" not in op_export:
            return False

        # BaseList2D object stored in "imexporter" key hold the settings
        obj_export = op_export["imexporter"]
        if obj_export is None:
            return False

        # Define the settings
        obj_export[6000] = True  # Separate mesh buy groups
        obj_export[c4d.OBJEXPORTOPTIONS_MATERIAL] = False  # Disable Materials

    # Export command
    if not c4d.documents.SaveDocument(
            doc_temp,
            path,
            c4d.SAVEDOCUMENTFLAGS_0,
            1030178):
        LOGGER.warning("Couldn't export the selection")
        c4d.documents.KillDocument(doc_temp)
        return False

    LOGGER.info("Export successful")
    c4d.documents.KillDocument(doc_temp)
    return True


def isSelectionPoly():
    """Get selected objects and return true if they are Polygons

    :return (bool): True if successful

    """
    selection = getDoc().GetActiveObjects(c4d.GETACTIVEOBJECTFLAGS_CHILDREN)

    for obj in selection:
        if obj.GetType() != 5100:
            return False
    return True


def getDoc():
    """Get the active document

    """
    return c4d.documents.GetActiveDocument()


def importObj(path):
    """Import the obj from the given path to the scene

    :path (str): Path to the obj file
    :return (bool): True if successful

    """
    import_plugin = c4d.plugins.FindPlugin(1030177, c4d.PLUGINTYPE_SCENELOADER)
    if import_plugin is None:
        LOGGER.debug("Import fail... Plugin is None")
        return False
    if not os.path.isfile(path):
        LOGGER.debug("Import fail.. Path doesn't exists")
        return False

    doc = getDoc()
    c4d.documents.MergeDocument(doc, path, c4d.SCENEFILTER_OBJECTS, None)

def getExePath():
    """Find location of the uvlayout.exe then write it to exe_location
    and return it

    :returns (str): Location of the uvlayout.exe

    """
    LOGGER.debug("Trying to find uvlayout.exe path...")
    write_path = os.path.join(
        c4d.storage.GeGetStartupPath(),
        "plugins",
        "c4d_uvlayout",
        "res",
        "exe_location"
        )
    if os.path.isfile(write_path):
        with open(write_path, "r") as exe_file:
            _path = exe_file.read()
            if os.path.isfile(_path):
                LOGGER.debug(_path)
                return _path

    exe_path = [
        "D:\\Program Files (x86)\\headus UVLayout v2 Professional\\uvlayout.exe",
        "C:\\Program Files (x86)\\headus UVLayout v2 Professional\\uvlayout.exe",
        "D:\\Program Files\\headus UVLayout v2 Professional\\uvlayout.exe",
        "C:\\Program Files\\headus UVLayout v2 Professional\\uvlayout.exe",
        ]
    for _path in exe_path:
        if os.path.isfile(_path):
            with open(write_path, "w") as exe_file:
                exe_file.write(_path)
            return _path

    c4d.gui.MessageDialog("Could not find uvlayout.exe path, please locate the uvlayout.exe")
    LOGGER.error("Could not find uvlayout.exe path, please locate the uvlayout.exe")
    _path = c4d.storage.LoadDialog(
        title="Select uvlayout.exe",
        force_suffix="exe"
        )
    if _path is not None:
        if _path[-12:] == "uvlayout.exe":
            with open(write_path, "w") as exe_file:
                exe_file.write(_path)
                return _path
    return None

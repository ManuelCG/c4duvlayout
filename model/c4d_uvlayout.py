#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: c4d_uvlayout.py
Author: Manuel Canavarros
Email: manuel.canavarros@gmail.com
Github: https://bitbucket.org/manuelcanavarrosgirard/
Description: Manages UV layout on plugin mode for cinema 4D

Copyright © 2017 Manuel Canavarros

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import logging
import os
import subprocess
from time import sleep

import c4d
from c4d.threading import C4DThread
from c4d_utils import exportSelection, isSelectionPoly, getDoc, importObj

# Configure Logger
LOGGER = logging.getLogger(__name__)
_HANDLER = logging.StreamHandler()
_FORMAT = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
_HANDLER.setFormatter(_FORMAT)
LOGGER.addHandler(_HANDLER)
LOGGER.setLevel(logging.DEBUG)


class C4DUVLayout(object):

    """ This object is responsible for all actions
    of UV Layout inside Cinema 4D
    """

    def __init__(self, uv_path):
        self.uv_path = uv_path
        self.watcher_folder = None
        self.uv_process = None

    def run(self, path=None):
        """Start UV layout in plugin mode using Subprocess.
        Then run _load function

        :path (str): Path for the directory containing the UV layout.exe
        :returns (bool): True if successful

        """
        path = path if path is not None else self.uv_path

        LOGGER.debug("Running UV Layout on the path %s", path)
        cwd = os.getcwd()
        os.chdir(self._getUVFolder())
        self.uv_process = subprocess.Popen(
            [path, '-plugin,poly', "tempc4d.obj"]
            )
        os.chdir(cwd)
        self._load()
        return True

    def send(self):
        """
        Export the selected objects to UVFolder and name it "temp",
        then rename it to "tempc4d.obj". This is done to avoid UVlayout
        to rename it to the .keep file before the export process it's
        fully complete

        :returns (bool): True if successful
        """

        if self.uv_process is None or self.uv_process.poll() is not None:
            c4d.gui.MessageDialog("Could not find any UV Layout Open")
            LOGGER.error("Could not find any UV Layout Open")
            return False

        if not isSelectionPoly():
            c4d.gui.MessageDialog("All your objects need to be "
                                  "polygons to be send to UV Layout")
            LOGGER.error("All your objects need to be "
                         "polygons to be send to UV Layout")
            return False

        path = os.path.join(self._getUVFolder(), 'temp')
        if not exportSelection(path):
            c4d.gui.MessageDialog("Nothing selected... Stopping send")
            LOGGER.error("Nothing selected... Stopping send")
            return False

        LOGGER.debug("Exporting selected objects to %s", self._getUVFolder())
        if os.path.isfile(os.path.join(self._getUVFolder(), 'tempc4d.obj')):
            LOGGER.debug("Removing old tempc4d.obj from UVFolder")
            os.remove(os.path.join(self._getUVFolder(), "tempc4d.obj"))

        LOGGER.debug("Renaming temp to tempc4d.obj")
        os.rename(path, os.path.join(self._getUVFolder(), "tempc4d.obj"))
        LOGGER.info("Objects sent to UV Layout")

        doc = getDoc()
        self.watcher_folder.poly_list = doc.GetActiveObjects(
            c4d.GETACTIVEOBJECTFLAGS_CHILDREN)

        return True

    @staticmethod
    def _getUVFolder():
        """
        Return the UV layout folder inside the c4d plugin folder
        If the folder doesn't exist create it

        :returns (str): Path to the c4d UV layout folder

        """
        path = os.path.join("plugins", "c4d_uvlayout", "temp")
        if not os.path.exists(
                os.path.join(
                    c4d.storage.GeGetStartupPath(),
                    path
                    )
            ):
            os.mkdir(os.path.join(c4d.storage.GeGetStartupPath(), path))
        return os.path.join(c4d.storage.GeGetStartupPath(), path)

    def _load(self):
        """Create a C4DWatch folder thread and start it

        """
        self.watcher_folder = C4DWatchFolder(
            self._getUVFolder(),
            self.uv_process
            )
        self.watcher_folder.Start()

    def endWatcher(self):
        """Ends C4DWatcherFolder thread"""

        if self.watcher_folder is not None:
            self.watcher_folder.End()


class C4DWatchFolder(C4DThread):

    """
    Watch the UVFolder for tempc4d.out if it founds
    the file rename it to tempc4din.obj and import it to the scene

    :uv_folder (str): Path to the UV layout folder in Cinema 4D startup path
    :uv_process: Subprocess containing the UVLayout task
    """

    def __init__(self, uv_folder, uv_process):
        super(C4DWatchFolder, self).__init__()
        self.uv_folder = uv_folder
        self.uv_process = uv_process
        self.poly_list = []

    def Main(self):
        """Check if tempc4d.out exist in uv_folder if it's true
        rename it to tempc4din.obj and import to the scene

        """
        temp_out = os.path.join(self.uv_folder, "tempc4d.out")
        while True:
            # Check if the End command was sent
            if self.TestBreak():
                self.closeUVLayout()
                LOGGER.debug("Ending C4D UVlayout folder wachter")
                break

            # Check if the subprocess was terminated
            if self.uv_process.poll() is not None:
                LOGGER.debug("Ending C4D UVlayout folder wachter")
                break

            sleep(0.2)
            # Check if temp_out exists
            if os.path.isfile(temp_out):
                LOGGER.debug("Found tempc4d.out, renaming it")

                if os.path.isfile(
                        os.path.join(
                            self.uv_folder,
                            "tempc4din.obj")):
                    LOGGER.debug("Deleting old tempc4din.obj")
                    os.remove(os.path.join(self.uv_folder, "tempc4din.obj"))

                os.rename(temp_out, os.path.join(
                    self.uv_folder, "tempc4din.obj"))

                # Import obj to the scene and swap UWV
                LOGGER.debug("Importing c4dtempin.obj")
                importObj(os.path.join(self.uv_folder, "tempc4din.obj"))
                self.transferUVW()

    def closeUVLayout(self):
        """Write "exit" to a file named tempc4d.cmd in the uv_folder
        to force the UV Layout to close

        :returns (boo): True if successful

        """
        exit_cmd = os.path.join(self.uv_folder, "tempc4d.cmd")
        with open(exit_cmd, 'w') as cmd:
            cmd.write("exit")

    def transferUVW(self):
        """Import the tempc4din.obj to the scene
        Then copy the UVW Tag from the imported polygons
        to the polygons in the scene

        """
        LOGGER.info("Starting UVW transfer")
        # Create ditc with UVW Tags from imported Polygons
        doc = getDoc()
        imported_objects = doc.GetActiveObjects(c4d.GETACTIVEOBJECTFLAGS_CHILDREN)
        imported_polys = []
        # If obj type its null get children and remove null
        for obj in imported_objects:
            if obj.GetType() == 5140:
                for children in obj.GetChildren():
                    imported_polys.append(children)
            imported_polys.append(obj)

        tag_dict = {}
        for obj in imported_polys:
            _name = obj.GetName()
            for tag in obj.GetTags():
                if tag.GetType() == 5671:
                    _tag = tag
            tag_dict[_name] = _tag

        # Remove old UVW Tag from original and insert new tag from imported
        LOGGER.debug("Transferring UVW Tag")
        for obj in self.poly_list:
            obj.KillTag(5671)
            _name = obj.GetName()
            try:
                obj.InsertTag(tag_dict[_name])
            except KeyError:
                LOGGER.error(
                    "Problably >>%s<< had a another name before, couldn't transferUVW to it",
                    _name
                    )

        # Remove imported
        LOGGER.debug("Removing imported obj")
        for obj in imported_objects:
            obj.Remove()
        LOGGER.info("UVW transfer completed")
